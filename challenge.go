package main

import (
	"fmt"
	"math"
	"strings"
)


func GetLocation(distances ...float64) (x, y float64) {

	const KENOBI_X = -500
	const KENOBI_Y = -200
	const SKYWALKER_X = 100
	const SKYWALKER_Y = -100
	const SATO_X = 500
	const SATO_Y = 100

	return calculateThreeCircleIntersection(KENOBI_X, KENOBI_Y, distances[0], SKYWALKER_X, SKYWALKER_Y, distances[1], SATO_X, SATO_Y, distances[2])		
}


func GetMessage(messages ...[]string) (msg string) {
	
	var length = getHighestLength(messages)
	
	var arr = make([]string, length)
	
	for _, message := range messages {
	
		for i := 0; i < len(message); i++ {
			
			if message[i] != "" {
				
				arr[i] = message[i];
				continue
			
			}	
	
		}
	
	}
	
	return strings.Join(arr," ")

}




func calculateThreeCircleIntersection(x0, y0, r0, x1, y1, r1, x2, y2, r2 float64) (x, y float64){

	const EPSILON = 0.000001

	var a, dx, dy, d, h, rx, ry, ponto2x, ponto2y float64 
	
	// dx e dy são as distâncias verticais e horizontais entre os centros do círculo
	dx = x1 - x0
	dy = y1 - y0
	
	// Determinando a distância em linha reta entre os centros.
	d = math.Sqrt((dy * dy) + (dx * dx))
	
	// Verificando a capacidade de solução
	if (d > (r0 + r1)) {
		// Sem solução. Os círculos não se cruzam
		return -999999999999, -999999999999
	}
	
	if (d < math.Abs(r0 - r1)) {
		// Sem solução. Um círculo está contido no outro 
		return -999999999999, -999999999999
	}
	
	/* "ponto2" é o ponto onde a linha através dos pontos de 
	 * intersecção do círculo cruzam a linha entre os centros do círculo
	 */
				
	// Determinando a distância do ponto 0 ao ponto 2
	a = ((r0 * r0) - (r1 * r1) + (d * d)) / (2.0 * d) 
	
	// Determinando as coordenadas do ponto 2 
	ponto2x = x0 + (dx * a/d)
	ponto2y = y0 + (dy * a/d)
	
	// Determinando a distância do ponto 2 até qualquer um dos pontos de interseção		
	h = math.Sqrt((r0 * r0) - (a * a ))

	// Agora, determinando os deslocamentos dos pontos de interseção do ponto 2
	rx = -dy * (h/d)
	ry = dx * (h/d)
	
	

	// Determinando os pontos de interseção absolutos
		
	var pontoIntersecao1x = ponto2x + rx
	var pontoIntersecao2x = ponto2x - rx
	var pontoIntersecao1y = ponto2y + ry
	var pontoIntersecao2y = ponto2y - ry
	
	
	fmt.Println("Interseção entre o círculo 1 e o círculo 2: (" , pontoIntersecao1x , "," , pontoIntersecao1y , ")" , " E (" , pontoIntersecao2x , "," , pontoIntersecao2y , ")")


	// Determinando se o círculo 3 se cruza em qualquer um dos pontos de intersecção acima
	var d1, d2 float64

	dx = pontoIntersecao1x - x2
	dy = pontoIntersecao1y - y2
	
	d1 = math.Sqrt((dy * dy) + (dx * dx))
	
	dx = pontoIntersecao2x - x2
	dy = pontoIntersecao2y - y2

	d2 = math.Sqrt((dy * dy) + (dx * dx))
	
	if math.Abs(d1 - r2) < EPSILON {
		fmt.Println("Interseção entre o círculo, círculo 2 e o círculo 3: (" , pontoIntersecao1x , "," , pontoIntersecao1y , ")")
		return pontoIntersecao1x, pontoIntersecao1y
	} else if math.Abs(d2 - r2) < EPSILON {
		fmt.Println("ERRO !")
		fmt.Println("Interseção entre o círculo, círculo 2 e o círculo 3: (" , pontoIntersecao2x , "," , pontoIntersecao2y , ")") //Aqui ocorreu um erro
	} else {
		fmt.Println("Interseção entre o círculo, círculo 2 e o círculo 3: NENHUMA")
	}
		

	return -999999999999, -999999999999
	
}


func getHighestLength(messages [][]string ) (nbr int) {


	var msgsLength int = len(messages)

	var lengths = make([]int, msgsLength)

		
	for i := 0; i < msgsLength; i++ {
		lengths[i] = len(messages[i])
	}
	
	var highest int = 0
	
	for _, length := range lengths {
	
		if highest == 0 {
			highest = length
		} else {
			if highest < length {
				highest = length	
			}
		}
    }
	
	return highest

}


func main() {

	var kbiMsg = []string{"este", "", "", "mensaje", ""}
	var swkrMsg = []string{"", "es", "", "", "secreto"}
	var satoMsg = []string{"este", "", "un", "", ""}

	fmt.Println(GetMessage( kbiMsg, swkrMsg, satoMsg))

	fmt.Println(GetLocation(670.820393249937, 200, 400)) // Valido
	fmt.Println(GetLocation(485.6956351461273, 266.08316369135423, 600.5)) // Valido
	fmt.Println(GetLocation(399, 427, 539)) // Invalido
	
}
