# NÍVEL 1

A primeira parte do desafio propões a descoberta das coordenadas (x e y) de uma fonte emissora de informações, com base na sua distância em relação a três receptores, cujas coordenadas são pré-estabelecidas.

A solução proposta utiliza como base para o cálculo o ponto de interseção entre três círculos. O raio de cada um dos círculos corresponde à distância entre o emissor e a fonte receptora.

O desenho abaixo ilustra a ideia da solução:

![three_circles_intersection](images/three_circles_intersection.png)

Essa primeira parte propõe a criação de duas funções em GoLang. A primeira função recebe um variadic do tipo float32 (foi utilizado o tipo float64 devido aos cálculos matemáticos envolvidos na solução proposta)
contendo as distâncias entre os receptores e a fonte emissora. Essa função retorna o ponto (x,y) - plano cartesiano de 2 dimensões - onde a fonte emissora se encontra.

A segunda função recebe também um variadic contendo arrays do tipo string, que contém as mensagens recebidas por cada receptor. Essa função percorre as strings contidas nos arrays,
desprezando strings vazias e compondo um novo array com strings validas à medida em que sao encontradas em suas devidas posições, sem repetição. Ao final, o array resultante é convertido para string e retornado pela função.

## Nível 1 - Como executar?

Na pasta raíz do projeto se encontra o arquivo *challenge.go*, criado para ser executado de forma standalone. Nele estão contidas as funções *GetLocation()* e *GetMessage()* solicitadas no desafio, as funções *calculateThreeCircleIntersection()* e 
*getHighestLength()* para uso interno, além da função *main()* para a execução dos testes.

O arquivo pode ser executado através do comando "go run challenge.go". Nesse caso, se faz necessário instalação dos binários no computador onde os testes serão realizados (link para o download: [Golang Download Page](https://golang.org/doc/install)). 
Alternativamente, pode-se copiar o conteúdo do arquivo em questão e executá-lo diretamente na página [A Tour of Go](https://tour.golang.org/welcome/1).


## Alguns exemplos de execução - funcao main()

### Obtendo a localização da fonte emissora:

func main() {  
&nbsp;&nbsp;fmt.Println(GetLocation(670.820393249937, 200, 400))	
}

### Onde:

- 670.820393249937 = distância entre o satélite Kenobi e a fonte emissora
- 200 = distância entre o satélite Skywalker e a fonte emissora
- 400 = distância entre o satélite Sato e a fonte emissora

Obs1: A ordem dos parâmetros de entrada deve ser respeitada: 1o Kenobi, 2o Skywalker, 3o Sato. Caso contrário, o cálculo não será corretamente efetuado  
Obs2.: Caso não seja possível calcular a interseção entre os 3 círculos, o valor -999999999999 será retornado, tanto para x quanto y


### Obtendo a mensagem:


func main() {  

&nbsp;&nbsp;var kbiMsg = []string{"este", "", "", "mensaje", ""}  
&nbsp;&nbsp;var swkrMsg = []string{"", "es", "", "", "secreto"}  
&nbsp;&nbsp;var satoMsg = []string{"este", "", "un", "", ""}

&nbsp;&nbsp;fmt.Println(GetMessage( kbiMsg, swkrMsg, satoMsg))

}


# NÍVEL 2

A segunda parte do desafio propõe a disponibilização de um endpoint em um provedor em nuvem de livre escolha. A ideia é que esteja disponível um método 
HTTP do tipo POST, através rota "/topsecret", que recebe alguns dados no corpo da requisição e retorna respostas com base nas execuções dos métodos acima mencionados.

Para atender a essa demanda, uma aplicação Web encontra-se disponível através do endereço: **[REMOVIDO]**

![postman_execution](images/postman_execution.png)


### CONTATOS

Ronie Dias Pinto  
Senior Software Engineer  
Linkedin: https://www.linkedin.com/in/ronie-dias  
E-mail: roniedias@yahoo.com  
Telefone: +55 (11) 98674-4103
